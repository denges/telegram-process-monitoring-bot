# Telegram Process Bot
This simple Telegram bot monitors running processes and sends broadcasts a notification to all active chats when the process finishes. A whitelist file contains all allowed Telegram usernames.
## Usage
* Add a process to be monitored: Write the PID to the file processes, for example

    echo [PID] > processes

* Send _/start_ to the bot to start a session in Telegram:

* Send _/list_ to the bot to retrieve a list of all monitored
    
* Send _/stop_ to the bot to stop receiving notifications: 

    
## Installation
Run _./install_ which installs all the necessary python libraries and initiates the setup

## Setup
Run _./setup_ to set up the bot. You will need a bot token and your telegram user name to do so.

## Running the bot
Run _python3 processBot.py_ after installation and setup
