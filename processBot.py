#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to monitor running processes.

This Bot uses the Updater class to handle the bot.
"""

import logging

from telegram.ext import Updater, CommandHandler

from time import sleep,time

#pid_exists from answer by congusbongus on https://stackoverflow.com/questions/568271/how-to-check-if-there-exists-a-process-with-a-given-pid-in-python
import  errno
import os
import sys

def pid_exists(pid):
    """Check whether pid exists in the current process table.
    UNIX only.
    """
    if pid < 0:
        return False
    if pid == 0:
        # According to "man 2 kill" PID 0 refers to every process
        # in the process group of the calling process.
        # On certain systems 0 is a valid PID but we have no way
        # to know that in a portable fashion.
        raise ValueError('invalid PID 0')
    try:
        os.kill(pid, 0)
    except OSError as err:
        if err.errno == errno.ESRCH:
            # ESRCH == No such process
            return False
        elif err.errno == errno.EPERM:
            # EPERM clearly means there's a process to deny access to
            return True
        else:
            # According to "man 2 kill" possible error values are
            # (EINVAL, EPERM, ESRCH)
            raise
    else:
        return True

#get_pname by nishparadox on https://stackoverflow.com/questions/32295395/how-to-get-the-process-name-by-pid-in-linux-using-python
import subprocess
def get_pname(id):
    p = subprocess.Popen(["ps -o cmd= {}".format(id)], stdout=subprocess.PIPE, shell=True)
    return p.communicate()[0].decode("utf-8").replace("\n","")
#End stackoverflow code

def prettySecString(sec):
    """
    returns a string of the form hh:mm:ss given sec seconds
    """
    sec = int(sec)
    h = sec // 3600
    sec = sec % 3600
    m = sec // 60
    sec = sec % 60
    if h < 10:
        h = "0" + str(h)
    else:
        h = str(h)
    if m < 10:
        m = "0" + str(m)
    else:
        m = str(m)
    if sec < 10:
        sec = "0" + str(sec)
    else:
        sec = str(sec)
    return h+":"+m+":"+sec

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

#sets with active chats and monitored processes
chat_ids = set()
process_ids = set()

#read allowedUsers
allowedUsers = set()
with open('allowedUsers', 'r') as file:
    data = file.readlines()
    for line in data:
        line = line.replace('\n', '')
        if line != '':
            allowedUsers.add(line)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    if update.effective_user.username in allowedUsers:
        update.message.reply_text('Hi! You will be notified when a monitored process is started. \n Use /list to see a list of all monitored processes.')
        global chat_ids
        chat_ids.add(update.message.chat_id)
    else:
        update.message.reply_text('Sorry, I guess we were just not meant to be. You do not have permission to use this bot.')

def listProcesses(update, context):
    if update.message.chat_id in chat_ids:
        if len(process_ids) == 0:
            update.message.reply_text('There are no monitored processes')
        else:
            if len(process_ids) == 1:
                update.message.reply_text('There is one monitored process: {}'.format("".join(["`"+n+"` with PID "+str(p) + "(started "+prettySecString(time() - t)+" ago)" for p,n,t in process_ids])))
            else:
                update.message.reply_text('There are {} monitored processes: \n *{}'.format( len(process_ids), "\n *".join(["`"+n+"` with PID "+str(p) + "(started "+prettySecString(time() - t)+" ago)" for p,n,t in process_ids])))
    else:
        update.message.reply_text('Use /start to start a conversation first.')

def stop(update, context):
    if update.message.chat_id in chat_ids:
        chat_ids.discard(update.message.chat_id)
        update.message.reply_text("You won't receive any further updates.")
    else:
        update.message.reply_text('Use /start to start a conversation first.')

def broadcast(updater,text):
    for chat in chat_ids:
        updater.bot.sendMessage(chat,text)


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    """Run bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    global process_ids
    token = "TOKEN"
    with open('token', 'r') as file:
        token = file.read().replace('\n','')
    updater = Updater(token, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("stop", stop))
    dp.add_handler(CommandHandler("list", listProcesses))


    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    while True:
        #Check whether there is any new processes to be monitored
        with open('processes', 'r') as file:
            data = file.readlines()
            for line in data:
                line = line.replace('\n', '')
                if line != '' and pid_exists(int(line)):
                    new = True
                    pid = int(line)
                    for p,_,_ in process_ids:
                        if p == pid:
                            new = False
                    if new:
                        n = get_pname(pid)
                        process_ids.add((pid,n,time()))
                        broadcast(updater,"New monitored process `"+n+"` with PID "+str(line)+" added.")
        
        #empty the file
        open('processes',"w").close()
        
        #check whether processes are all still alive
        finishedProcesses = set()
        for (p,n,t) in process_ids:
            if not pid_exists(p):
                finishedProcesses.add((p,n,t))
                broadcast(updater,"Process `{}` with PID {} finished after {}.".format(n,str(p),prettySecString(time()-t)))
        
        process_ids.difference_update(finishedProcesses)
        # Sleep a little
        sleep(1)

    # Block until you press Ctrl-C or the process receives SIGINT, SIGTERM or
    # SIGABRT. This should be used most of the time, since start_polling() is
    # non-blocking and will stop the bot gracefully.
    #updater.idle()


if __name__ == '__main__':
    main()
